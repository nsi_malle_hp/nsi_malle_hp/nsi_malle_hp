#  coding: utf-8 

"""
Ce programme simule un retour de billet de différentes boutiques sur le chemin
de traverse de l'univers d'Harry Potter. La boutique 1 rend des billets d'après 
une banque illimitée, la boutique 2 a quand à elle un nombre de billet défini
et la boutique trois convertie et réduit au minimum le nombre de noises, 
mornilles et gallions (la monnaie du monde des sorciers).
Bon voyage sur le chemin de traverse !

Authors : LEFEVRE Pierrick, LALAUDE Martin, FRANCOIS Valentine
Version : v_12
URL Gitlab : https://gitlab.com/nsi_malle_hp/nsi_malle_hp/nsi_malle_hp
"""

# Fonctions partie A
def boutique_1(sommes_a_rendre):
    """
    Calcule la quantité d'argent à rendre en fonction d'une somme donnée par
    l'utilisateur.
    
    Entrée : un entier qui correspond à l'argent à rendre
    Sortie : combien de billets le marchand doit rendre
    """
    valeurs = (500, 200, 100, 50, 20, 10, 5, 2, 1)
    resultat = {}

    for valeur in valeurs:
        if sommes_a_rendre >= valeur:
            quantite = sommes_a_rendre // valeur
            resultat[valeur] = quantite
            sommes_a_rendre -= valeur * quantite
    return resultat

def affichage_resultat(dico):
    """
    Permet un affichage correct pour les fonctions tests_boutique_1 et
    utilisateur_boutique_1.
    
    Entrée : un dictionnaire des sommes a rendre
    Sortie : la quantité et la valeur des billets a rendre
    """
    for quantite, valeur in dico.items():
        print(f"{valeur} billet(s) de {quantite} €.")

def test_boutique_1():
    """
    Effectue et affiche les tests demandés à l'aide des fonctions boutique_1()
    et affichage_boutique_1().
    
    Entrée : Rien
    Sortie : l'affichage de la fonction affichage_boutique_A en fonction des
    tests définis.
    """
    sommes_a_rendre = (60, 63, 231, 899)
    print("\nPour 0 € je vous rends :\nJe vous rends rien.")
    for somme in sommes_a_rendre:
        print(f"\nPour {somme} € je vous rends :")
        affichage_resultat(boutique_1(somme))
        
def utilisateur_boutique_1():
    """
    Effectue et affiche les tests demandés à l'aide des fonctions boutique_1(sommes_a_rendre)
    et affichage_boutique_A().
    entrée : rien
    sortie : l'affiche dans la console du nombre de billets à rendre
    """
    somme_a_rendre = int(input("\nEntrez la somme à rendre en euros : "))
    print(f"pour {somme_a_rendre}€, je vous rend:")

    affichage_resultat(boutique_1(somme_a_rendre))    

    
# Fonctions partie B
def boutique_2(a_rendre):
    """
    Calcule la quantité de billets à rendre en fonction d'une somme donnée par
    l'utilisateur.
    
    Entrée : un entier qui correspond à l'argent à rendre
    Sortie : un dictionnaire contenant les types de billet ou pièce en clé et 
    les quantités en valeur.
    """
    
    liste_billets = (200, 100, 100, 100, 50, 20, 10, 5, 2, 2, 2, 2, 2)
    billets_restants = [200, 100, 100, 100, 50, 20, 10, 5, 2, 2, 2, 2, 2]
    argent_rendu = {200 : 0, 100 : 0, 50 : 0, 20 : 0, 10 : 0, 5 : 0, 2 : 0}
    somme_exacte = True
    if a_rendre > 600:
        print("Désolé je n'ai pas assez pour vous rembourser : je n'ai que 600"
              "€. Je ne vous rends donc que ça.")
    else :          
        for billet in liste_billets:
            if a_rendre >= billet:
                argent_rendu[billet] += 1
                a_rendre -= billet
                billets_restants.remove(billet)
        if a_rendre != 0:
            somme_exacte = False
            
    return argent_rendu, billets_restants, somme_exacte  

def execution_affichage(a_rendre):
    """
    Passe en argument de la fonction affichage() les valeurs de sortie de la 
    fonction boutique_2().
    
    Entrée : le dictionnaire de sortie de la fonction boutique_2
    Sortie : l'execution de la fonction affichage()

    """
    
    argent_rendu, billets_restants, somme_exacte = boutique_2(a_rendre)
    affichage_resultat(argent_rendu, billets_restants, somme_exacte)

def affichage_resultat(argent_rendu, billets_restants, somme_exacte):
    """
    Affiche dans la console la quantité de billets pour des valeurs choisies
    à partie des calculs de la fonction boutique_2().
    
    Entrée : un dictionnaire contenant les billets et leur quantité respective
             un liste avec les billets restants dans la caisse (BONUS)
             un booléen qui dit si le client a recu tout son argent (BONUS)
    Sortie : l'affichage dans la console des billets et de leur quantité
    """
    for valeur, quantite in argent_rendu.items():
        if quantite != 0:
            print(f"{quantite} billet(s) de {valeur} €")
    if somme_exacte == False:
        billets_restants.sort()
        print(f"plus 1 billet de {billets_restants[0]} € car je ne peux pas vous"
              " rendre la somme exacte.")     
    
def tests_boutique_2():
    """
    Effectue et affiche les tests demandés à l'aide des fonctions boutique_2()
    et affichage_boutique_2().
    
    Entrée : Rien
    Sortie : l'affichage de la fonction affichage_boutique_2 en fonction des
    tests définis.
    """
    
    test = (17, 68, 231, 497)
    print("\nPour 0 € je ne vous rends rien.")
    for valeur in test:
        print(f"\nPour {valeur} € je vous rends :")
        execution_affichage(valeur)   
    print("\nPour 842 € je vous rends le maximum que j'ai : 600 €. Désolé.")
   

def utilisateur_boutique_2():
    """
    Effectue et affiche les quantités de chaque billet à l'aide des fonctions 
    boutique_2() et affichage_boutique_2() pour une somme donnée par
    l'utilisateur.
    
    Entrée : Rien
    Sortie : l'affichage de la fonction affichage_boutique_2() en fonction de 
    la somme choisie par l'utilisateur.
    """
    
    argent = int(input("\nCombien d'argent le commerçant doit-il rendre ? : "))
    print("\nJe vous rends :")
    execution_affichage(argent)


# Fonctions partie C
def boutique_3(gallion, mornille, noise):
    '''
    Caclule le nombre de gallions, mornilles, noises,
    qu'Olivander doit rendre à Harry, mais de sorte a ce qu'il rende 
    le minimum de pièces possibles.
    
    Entrée : 3 entiers, le nombre de gallions, mornilles et noises.
    Sortie : 3 entiers, le nombre de gallions, mornilles et noises de façon 
    optimisée.
    '''
    while noise >= 27 or mornille >= 17:
        if noise >= 27:
            noise -= 27
            mornille += 1
        else:
            mornille -= 17
            gallion += 1
    return gallion, mornille, noise                   

def utilisateur_boutique_3(boutique_3):
    '''
    Renvoie le nombre de gallions, mornilles, noises, calculés par la fonction
    boutique_3 avec IHM agréable.
    
    Entrée : 3 entiers, le nombre de gallions, mornilles et noiss.
    Sortie : Une chaine de caractères et 3 entiers, le nombre de gallions,
    mornilles et noises de façon optimisée, calculé par la fonction boutique_3.
    '''
    gallion = int(input("\nCombien de gallion(s) Olivander doit rendre : "))
    mornille = int(input("Combien de mornille(s) Olivander doit rendre : "))
    noise = int(input("Combien de noise(s) Olivander doit rendre : "))

    gallion = boutique_3(gallion, mornille, noise)[0]
    mornille = boutique_3(gallion, mornille, noise)[1]
    noise = boutique_3(gallion, mornille, noise)[2]

    return f"\nJe vous rends {gallion} gallion(s), {mornille} mornille(s) et {noise} noise(s)"

def test_boutique_3():
    '''
    Fait les tests demandés, avec la fonction boutique_3.
    
    Entrée : Rien
    Sortie : Une chaine de caractères et 3 entiers, le nombre de gallions,
    mornilles et noises de façon optimisée des tests, caluclé par la fonction
    boutique_3.
    '''
    liste_test = [[0, 0, 0], [0, 0, 654], [0, 23, 78], [2, 11, 9], [7, 531, 451]]
    for test in liste_test:
        gallion = test[0]
        mornille = test[1]
        noise = test[2]
        gallion_2, mornille_2, noise_2 = boutique_3(gallion, mornille, noise)
        print(f"\nPour le test avec {gallion} gallion(s), {mornille} mornille(s)"
              f"et {noise} noises, je vous rend {gallion_2} gallion(s)," 
              f"{mornille_2} mornille(s) et {noise_2} noise(s).")


# IHM globale
def ihm_globale():
    '''
    Permet à l'utilisateur de parcourir le chemin de traverse et ses boutiques.
    
    Entrée : Rien
    Sortie : Des chaines de caractères et input pour comprendre et parcourir le
    Chemin de traverse et les 3 boutiques.
    '''

    reponse = input("\nNous sommes sur le Chemin de Traverse. Voulez-vous aller "
                    "dans la boutique 1, 2 ou 3 ? Toute réponse autre que 1, 2 "
                    "ou 3 vous fera sortir : ")
    while reponse in ("1", "2", "3"):
        if reponse == "1":
            rep_boutique = input("\nVoulez-vous le test (1) ou entrer vos propres valeurs (2)? Toute réponse autre que 1 ou 2 vous fera retourner sur le Chemin de Traverse : ")
            while rep_boutique in ("1", "2"):
                if rep_boutique == "1":
                    test_boutique_1()
                elif rep_boutique == "2":
                    utilisateur_boutique_1()
                rep_boutique = input("\nEntrez 1 pour le test, 2 pour l'affichage, ou autre chose si vous avez fini : ")
        elif reponse == "2":
            rep_boutique = input("\nVoulez-vous le test (1) ou entrer vos propres valeurs (2)? Toute réponse autre que 1 ou 2 vous fera retourner sur le Chemin de Traverse : ")
            while rep_boutique in ("1", "2"):
                if rep_boutique == "1":
                    tests_boutique_2()
                elif rep_boutique == "2":
                    utilisateur_boutique_2()
                rep_boutique = input("\nEntrez 1 pour le test, 2 pour l'affichage, ou autre chose si vous avez fini : ")
        elif reponse == "3":
            rep_boutique = input("\nVoulez-vous le test (1) ou entrer vos propres valeurs (2)? Toute réponse autre que 1 ou 2 vous fera retourner sur le Chemin de Traverse : ")
            while rep_boutique in ("1", "2"):
                if rep_boutique == "1":
                    test_boutique_3()
                elif rep_boutique == "2":
                    print(utilisateur_boutique_3(boutique_3))
                rep_boutique = input("\nEntrez 1 pour le test, 2 pour l'affichage, ou autre chose si vous avez fini : ")
        reponse = input("\nVous êtes sur le Chemin de Traverse, où voulez-vous aller maintenant ? ")
    return "\nAu revoir Harry !!!"

# execution du programme
print(ihm_globale())
