# -*- coding: utf-8 -*-

# 1 gallion = 17 mornilles
# 1 mornille = 27 noises

def boutique_3(gallion, mornille, noise):
    '''
    Caclule le nombre de gallions, mornilles, noises,
    qu'Olivander doit rendre à Harry, mais de sorte a ce qu'il rende 
    le minimum de pièces possibles.
    
    Entrée : 3 entiers, le nombre de gallions, mornilles et noises.
    Sortie : 3 entiers, le nombre de gallions, mornilles et noises de façon 
    optimisée.
    '''
    while noise >= 27 or mornille >= 17:
        if noise >= 27:
            noise -= 27
            mornille += 1
        elif mornille >= 17:
            mornille -= 17
            gallion += 1
    return gallion, mornille, noise                   

def affichage_boutique_3(boutique_3):
    '''
    Renvoie le nombre de gallions, mornilles, noises, calculés par la fonction
    boutique_3 avec IHM agréable.
    
    Entrée : 3 entiers, le nombre de gallions, mornilles et noiss.
    Sortie : Une chaine de caractères et 3 entiers, le nombre de gallions,
    mornilles et noises de façon optimisée, calculé par la fonction boutique_3.
    '''
    gallion = int(input("Combien de gallions Olivander doit rendre : "))
    mornille = int(input("Combien de mornilles Olivander doit rendre : "))
    noise = int(input("Combien de noises Olivander doit rendre : "))

    gallion = boutique_3(gallion, mornille, noise)[0]
    mornille = boutique_3(gallion, mornille, noise)[1]
    noise = boutique_3(gallion, mornille, noise)[2]

    return f"Je vous rend {gallion} gallion(s), {mornille} mornille(s) et {noise} noise(s)"

#print(affichage_boutique_3(boutique_3))

def test_boutique_3():
    '''
    Fait les tests demandés, avec la fonction boutique_3.
    
    Entrée : Rien
    Sortie : Une chaine de caractères et 3 entiers, le nombre de gallions,
    mornilles et noises de façon optimisée des tests, caluclé par la fonction
    boutique_3.
    '''
    liste_test = [[0, 0, 0], [0, 0, 654], [0, 23, 78], [2, 11, 9], [7, 531, 451]]
    for test in liste_test:
        gallion = test[0]
        mornille = test[1]
        noise = test[2]
        gallion_2, mornille_2, noise_2 = boutique_3(gallion, mornille, noise)
        print(f"Pour le test avec {gallion} gallion(s), {mornille} mornille(s)"
              f"et {noise} noises, je vous rend {gallion_2} gallion(s)," 
              f"{mornille_2} mornille(s) et {noise_2} noise(s).")
    return "Au revoir !"
#print(test_boutique_3())
        