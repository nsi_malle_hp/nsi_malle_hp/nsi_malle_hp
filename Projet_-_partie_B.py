# -*- coding: utf-8 -*-
def boutique_2(a_rendre):
    """
    Calcule la quantité de billets à rendre en fonction d'une somme donnée par
    l'utilisateur.
    
    Entrée : un entier qui correspond à l'argent à rendre
    Sortie : un dictionnaire contenant les types de billet ou pièce en clé et 
    les quantités en valeur.
    """
    
    liste_billets = [200, 100, 100, 100, 50, 20, 10, 5, 2, 2, 2, 2, 2]
    billets_restants = [200, 100, 100, 100, 50, 20, 10, 5, 2, 2, 2, 2, 2]
    argent_rendu = {200 : 0, 100 : 0, 50 : 0, 20 : 0, 10 : 0, 5 : 0, 2 : 0}
    somme_exacte = True
    if a_rendre > 600:
        print("Désolé je n'ai pas assez pour vous rembourser : je n'ai que 600"
              "€. Je ne vous rend donc que ça.")
    else :          
        for billet in liste_billets:
            if a_rendre >= billet:
                argent_rendu[billet] += 1
                a_rendre -= billet
                billets_restants.remove(billet)
        if a_rendre != 0:
            somme_exacte = False
            
    return argent_rendu, billets_restants, somme_exacte  



def affichage(argent_rendu, billets_restants, somme_exacte):
    """
    Affiche dans la console la quantité de billets pour des valeurs choisies
    à partie des calculs de la fonction boutique_2().
    
    Entrée : un dictionnaire contenant les billets et leur quantité respective
             un liste avec les billets restants dans la caisse (BONUS)
             un booléen qui dit si le client a recu tout son argent (BONUS)
    Sortie : l'affichage dans la console des billets et de leur quantité
    """
    for valeur, quantite in argent_rendu.items():
        if quantite != 0:
            print(f"{quantite} billets de {valeur} €")
    if somme_exacte == False:
        billets_restants.sort()
        print(f"plus 1 billet de {billets_restants[0]} € car je ne peux pas vous"
              " rendre la somme exacte.")     
    

def execution_affichage(a_rendre):
    """
    Passe en argument de la fonction affichage() les valeurs de sortie de la 
    fonction boutique_2().
    
    Entrée : le dictionnaire de sortie de la fonction boutique_2
    Sortie : l'execution de la fonction affichage()

    """
    
    argent_rendu, billets_restants, somme_exacte = boutique_2(a_rendre)
    affichage(argent_rendu, billets_restants, somme_exacte)
        
        
def test_boutique_2():
    """
    Effectue et affiche les tests demandés à l'aide des fonctions boutique_2()
    et affichage_boutique_2().
    
    Entrée : Rien
    Sortie : l'affichage de la fonction affichage_boutique_2 en fonction des
    tests définis.
    """
    
    test = (17, 68, 231, 497,)
    print("\nPour 0 € je ne vous rend rien.")
    for valeur in test:
        print(f"\nPour {valeur} € je vous rend :")
        execution_affichage(valeur)   
    print("\nPour 842 € je vous rend le maximum que j'ai : 600 €. Désolé.")
   

def affichage_utilisateur_boutique_2():
    """
    Effectue et affiche les quantités de chaque billet à l'aide des fonctions 
    boutique_2() et affichage_boutique_2() pour une somme donnée par
    l'utilisateur.
    
    Entrée : Rien
    Sortie : l'affichage de la fonction affichage_boutique_2() en fonction de 
    la somme choisie par l'utilisateur.
    """
    
    argent = int(input("\nCombien d'argent le commerçant doit-il rendre ? : "))
    print("\nJe vous rend :")
    execution_affichage(argent)
    
    
test_boutique_2()
affichage_utilisateur_boutique_2()
    



