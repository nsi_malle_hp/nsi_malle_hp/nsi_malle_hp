def boutique_1(sommes_a_rendre):
    """
    Calcule la quantité d'argent à rendre en fonction d'une somme donnée par
    l'utilisateur.
    
    Entrée : un entier qui correspond à l'argent à rendre
    Sortie : combien de billets le marchand doit rendre
    """
    valeurs = [500, 200, 100, 50, 20, 10, 5, 2, 1]
    resultat = {}

    for valeur in valeurs:
        if sommes_a_rendre >= valeur:
            quantite = sommes_a_rendre // valeur
            resultat[valeur] = quantite
            sommes_a_rendre -= valeur * quantite
    return resultat

def affichage(dico):
    for quantite, valeur in dico.items():
        print(f"Je vous rends {valeur} billets de {quantite} €.")

def test_boutique_1():
    """
    Effectue et affiche les tests demandés à l'aide des fonctions boutique_2()
    et affichage_boutique_A().
    
    Entrée : Rien
    Sortie : l'affichage de la fonction affichage_boutique_A en fonction des
    tests définis.
    """
    sommes_a_rendre = (0, 60, 63, 231, 899)
    for somme in sommes_a_rendre:
        print(f"\nPour {somme} € je vous rend :")
        affichage(boutique_1(somme))
        
def affichage_boutique_A():
    """
    Effectue et affiche les tests demandés à l'aide des fonctions boutique_1(sommes_a_rendre)
    et affichage_boutique_A().
    entrée : rien
    sortie : l'affiche dans la console du nombre de billets à rendre
    """
    somme_a_rendre = int(input("Entrez la somme à rendre en euros : "))
    print(f"pour {somme_a_rendre}€, je vous rend:")

    affichage(boutique_1(somme_a_rendre))    

    print("\nVous pouvez maintenant passer au magasin suivant.")

test_boutique_1()
affichage_boutique_A()
